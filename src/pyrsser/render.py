import html.parser
import queue
import re

import jinja2
from markupsafe import escape


def get_env(template_dir):
    loaders = []
    if template_dir:
        loaders.append(jinja2.FileSystemLoader(template_dir))
    loaders.append(jinja2.PackageLoader('pyrsser'))
    env = jinja2.Environment(loader=jinja2.ChoiceLoader(loaders))
    env.filters['strip_non_tg_tags'] = strip_non_tg_tags
    return env


TELEGRAM_MAX_LENGTH = 4096


def render_entry(entry, template_name, env):
    template = env.get_template(template_name)
    rendered = template.render(entry=entry)
    if len(rendered) > TELEGRAM_MAX_LENGTH:
        rendered = (
            '<a href="{link}">{title}</a>\n'
            'Entry has too long summary. Click the link below to view full entry'
            .format(link=entry['link'], title=entry['title'])
        )
    rendered = re.sub('\n{3,}', '\n\n', rendered).strip()
    return rendered


class NonTGStripper(html.parser.HTMLParser):
    TELEGRAM_SUPPORTED_TAGS = {
        'a': ['href'],
        'b': [],
        'strong': [],
        'i': [],
        'em': [],
        'pre': [],
        'code': [],
    }
    NEWLINE_ENDING_TAGS = {'p', 'li'}

    def __init__(self):
        super(NonTGStripper, self).__init__()
        self.data = []
        self.inside_tag = False
        self.skipped_tags = queue.LifoQueue()

    def length(self):
        return sum(map(len, self.data))

    def handle_starttag(self, tag, attrs):
        if tag in self.TELEGRAM_SUPPORTED_TAGS:
            if self.inside_tag:
                self.skipped_tags.put(tag)
                return
            attrs = ' '.join(
                '{attr}="{value}"'.format(attr=attr, value=value)
                for attr, value in attrs
            )
            if attrs:
                attrs = ' ' + attrs
            self.data.append('<{tag}{attrs}>'.format(tag=tag, attrs=attrs))
            self.inside_tag = True
        if tag == 'br':
            self.data.append('\n')

    def handle_endtag(self, tag):
        if tag in self.TELEGRAM_SUPPORTED_TAGS:
            if self.inside_tag:
                if not self.skipped_tags.empty():
                    self.skipped_tags.get()
                    return
            self.data.append('</{tag}>'.format(tag=tag))
            self.inside_tag = False
        if tag in self.NEWLINE_ENDING_TAGS:
            self.data.append('\n')
        if tag == 'p':
            self.data.append('\n')

    def handle_data(self, data):
        data = re.sub(r'\s+', ' ', data.strip('\n'))
        self.data.append(escape(data))


def strip_non_tg_tags(text):
    parser = NonTGStripper()
    parser.feed(text)
    return ''.join(parser.data)
