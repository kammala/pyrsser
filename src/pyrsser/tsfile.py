import csv
import datetime as dt
import logging
from functools import total_ordering

logger = logging.getLogger(__name__)

FIELDS = ['id', 'ts']
DT_FORMAT = '%Y-%m-%d %H:%M:%S%z'


@total_ordering
class LastItem:
    def __init__(self, ts, id):
        self.ts = ts
        self.id = id

    @classmethod
    def from_entry(cls, entry):
        return cls(
            ts=dt.datetime(*entry['published_parsed'][:6], tzinfo=dt.timezone.utc),
            id=entry['id'],
        )

    def __lt__(self, other):
        return self.ts < other.ts

    def __eq__(self, other):
        return self.id == other.id and self.ts == other.ts

    def as_dict(self):
        return {
            'id': self.id,
            'ts': self.ts.strftime(DT_FORMAT),
        }


def get_ts(ts_file):
    try:
        with open(ts_file) as _ts_file:
            reader = csv.DictReader(_ts_file, fieldnames=FIELDS)
            data = next(reader, None)
            if data:
                return LastItem(
                    id=data['id'],
                    ts=dt.datetime.strptime(data['ts'], DT_FORMAT),
                )
            logger.info('Empty timestamp file')
    except OSError:
        logger.warning('Error while openning timestamp file: %s', ts_file)


def set_ts(ts_file, item):
    try:
        with open(ts_file, mode='w') as _ts_file:
            writer = csv.DictWriter(_ts_file, FIELDS)
            writer.writerow(item.as_dict())
    except OSError:
        logger.warning('Error while openning timestamp file: %s', ts_file)
