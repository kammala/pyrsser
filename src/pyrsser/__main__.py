import argparse
import logging.config
import os
import ssl

import feedparser
import json
from pyrsser.bot import get_bot_token, get_sender, configure_proxy
from pyrsser.render import render_entry, get_env
from pyrsser.tsfile import get_ts, set_ts, LastItem

logger = logging.getLogger(__name__)
VERBOSITY = {
    0: logging.CRITICAL,
    1: logging.ERROR,
    2: logging.WARNING,
    3: logging.INFO,
    4: logging.INFO,
    5: logging.DEBUG,
}


class UnverifiedSSLContextManager:
    def __init__(self, verify):
        self.verify = verify

    def __enter__(self):
        self._old = ssl._create_default_https_context
        if not self.verify:
            ssl._create_default_https_context = ssl._create_unverified_context

    def __exit__(self, exc_type, exc_val, exc_tb):
        ssl._create_default_https_context = self._old


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('url')
    parser.add_argument('--chat-id', required=True)
    parser.add_argument('--token-file', required=True)
    parser.add_argument('--template-name', dest='template', default='default.html.jinja2')
    parser.add_argument('--template-dir', dest='template_dir')
    parser.add_argument('--ts-file')
    parser.add_argument('--disable-notifications', action='store_true', default=False)
    parser.add_argument('-w', '--with-web-preview', dest='disable_web_preview', action='store_false', default=True)
    parser.add_argument(
        '-v', '--verbose',
        dest='verbosity',
        action='append_const',
        const=1,
        default=[logging.ERROR, logging.WARNING],
    )
    parser.add_argument('-q', '--quiet', dest='verbosity', action='store_const', const=[])
    parser.add_argument('-k', '--no-verify', dest='verify', action='store_false', default=True)

    args = parser.parse_args()
    verbosity = len(args.verbosity)

    loglevel = VERBOSITY.get(verbosity, logging.WARNING)
    log_config_file = os.environ.get('LOG_CONFIG')
    if log_config_file:
        try:
            with open(log_config_file) as log_config:
                logging.config.dictConfig(json.loads(log_config.read()))
        except OSError:
            logging.basicConfig(level=loglevel)
            logger.warning("Can't parse logging configuration")
    else:
        logging.basicConfig(level=loglevel)
        logger.debug('Using fallback logging settings')

    logger.debug('Configuring proxy')
    configure_proxy()

    last_item = None
    ts_file = args.ts_file
    if ts_file:
        last_item = get_ts(ts_file)

    token = get_bot_token(args.token_file)
    if not token:
        logger.error("Can't get bot token")
        raise SystemExit(1)

    sender = get_sender(token, args.chat_id, args.disable_notifications, args.disable_web_preview)

    template = args.template
    env = get_env(args.template_dir)

    with UnverifiedSSLContextManager(args.verify):
        feed = feedparser.parse(args.url)
    if feed.bozo:
        if isinstance(feed.bozo_exception, (feedparser.ThingsNobodyCaresAboutButMe,)):
            logger.info(
                'Parsing warnings: %s (%s)',
                feed.bozo_exception,
                feed.bozo_exception.__class__.__name__,
            )
        elif isinstance(feed.bozo_exception, (OSError,)):
            logger.critical('Incorrect feed: %s', feed.bozo_exception)
            raise SystemExit(2)
        else:
            logger.warning(
                'Unexpected parsing error: [%s] %s',
                feed.bozo_exception.__class__.__name__,
                feed.bozo_exception,
            )

    process(sender, last_item, ts_file, template, env, feed)


def process(sender, last_item, ts_file, template, env, feed):
    item = None
    for entry in reversed(feed['entries']):
        item = LastItem.from_entry(entry)
        if last_item is not None and item <= last_item:
            logger.debug('Ignoring item less than last sent')
            continue
        text = render_entry(entry, template, env)
        sender(text=text)
    if (
            ts_file
            and item is not None
            and (last_item is None or item > last_item)
    ):
        logger.debug('Updating timestamp file')
        set_ts(ts_file, item)


if __name__ == '__main__':
    main()
