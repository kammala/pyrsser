import logging
import os

import telepot

logger = logging.getLogger(__name__)


def get_bot_token(token_file):
    try:
        with open(token_file) as _token:
            token = _token.readline().strip()
            return token
    except OSError:
        logger.warning('Error while opening bot token file: %s', token_file)


def configure_proxy():
    import urllib3
    import telepot.api

    proxy_url = os.environ.get('https_proxy', os.environ.get('http_proxy'))

    if proxy_url:
        telepot.api._pools = {
            'default': urllib3.ProxyManager(
                proxy_url=proxy_url,
                num_pools=3,
                maxsize=10,
                retries=False,
                timeout=30,
            ),
        }

        telepot.api._onetime_pool_spec = (
            urllib3.ProxyManager,
            {
                'proxy_url': proxy_url,
                'num_pools': 1,
                'maxsize': 1,
                'retries': False,
                'timeout': 30.
            }
        )


def get_sender(token, chat, disable_notifications=False, disable_web_preview=True):
    bot = telepot.Bot(token=token)

    def sender(text):
        try:
            bot.sendMessage(
                chat_id=chat,
                disable_notification=disable_notifications,
                disable_web_page_preview=disable_web_preview,
                parse_mode='html',
                text=text,
            )
        except telepot.exception.TelegramError:
            logger.exception('Server returned error for %s', text)
        except Exception:
            logger.exception('Sending failed')

    return sender
